import os
import codecs
import json

class MySettings(object):
    def __init__(self, SettingsFile=None):
        try:
            with codecs.open(SettingsFile, encoding="utf-8-sig", mode="r") as f:
                self.__dict__ = json.load(f, encoding="utf-8")
        except:
            self.ClipboardCopyCommand = "!c"
            self.ClipboardCopyCooldown = 0
            self.ClipboardCopyPermission = "moderator"
            self.ClipboardCopyInfo = ""
            self.ClipboardPasteCommand = "!v"
            self.ClipboardPasteCooldown = 0
            self.ClipboardPastePermission = "regular"
            self.ClipboardPasteInfo = ""
            self.CurrentSongCommand = "!song"
            self.CurrentSongCooldown = 0
            self.CurrentSongPermission = "everyone"
            self.CurrentSongInfo = ""
            self.RandlineTextCommand = "!8ball"
            self.RandlineTextCooldown = 0
            self.RandlineTextPermission = "regular"
            self.RandlineTextInfo = ""
            self.RaidCommand = "!8ball"
            self.RaidCooldown: 0
            self.RaidInfo: ""
            self.RaidPermission: "moderator"

    def Reload(self, jsondata):
        self.__dict__ = json.loads(jsondata, encoding="utf-8")
        return

    def Save(self, SettingsFile):
        try:
            with codecs.open(SettingsFile, encoding="utf-8-sig", mode="w+") as f:
                json.dump(self.__dict__, f, encoding="utf-8")
            with codecs.open(SettingsFile.replace("json", "js"), encoding="utf-8-sig", mode="w+") as f:
                f.write("var settings = {0};".format(json.dumps(self.__dict__, encoding='utf-8')))
        except:
            Parent.Log(ScriptName, "Failed to save settings to file.")
        return