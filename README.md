# HoodedEthereal's Chatbot
https://www.twitch.tv/hoodedethereal

## Commands

### !c
Copies the message typed right after **!c** and stores it. \
This message is lost when it gets overwritten or when the script gets reloaded.

### !v
Pastes the message stored by **!c**, both of these commands may have different\
cooldowns and permissions.

### !song
Sends the Spotify songname from Snip in chat. If no songs are playing, nothing\
will be sent. The file path can be changed in *Hooded_StreamlabsStstem.py* by\
changing the field variable `songPath`.

### !8ball
Picks a message from the json file based on the probability value linked with it.\
The file path can be changed in *Hooded_StreamlabsStstem.py* by changing the\
field variable `resourcesEightBallPath`. Json file can be found in *Resources/8ball.json*.\
Json file can be updated in real time.
###### The test 'Yes' has a 40/760 chance of being sent by the bot.
```
{
    "Eightball":
    [
        {
            "value": 40,
            "text": "Yes"
        },
        {
            "value": 60,
            "text": "No"
        },
        {
            "value": 160,
            "text": "Maybe"
        },
        {
            "value": 500,
            "text": "KEKW"
        }
    ]
}
```

### !raid
Sends both raid commands in chat in two seperate messages for subs and for non-subs.\
The messages can be changed in *Hooded_StreamlabsStstem.py* by changing the field\
variables `raidText1` and `raidText2`.


