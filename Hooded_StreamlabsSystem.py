import os
import sys
import json
import random
sys.path.append(os.path.join(os.path.dirname(__file__), "lib"))

from StandardSettings_Module import MySettings

ScriptName = "HoodedEthereal script"
Website = "https://www.twitch.tv/hoodedethereal"
Description = "HoodedEthereal specific fancy stuff hell ye"
Creator = "Gauntbrother"
Version = "1.0"

global MySettings
SettingsFile = os.path.join(os.path.dirname(__file__), "Settings\settings.json")
clipboard = ""

#Paths
songPath = "D:\\Program Files (x86)\\Snip-v7.0.5\\Snip\\Snip.txt"
resourcesEightBallPath = "C:\Users\gaunt\AppData\Roaming\Streamlabs\Streamlabs Chatbot\Services\Scripts\hoodedethereal-chatbot\Resources\8ball.json"

#Values
raidText1 = "hooded11HoodedLove ratJAM The mischief has arrived! ratJAM hooded11HoodedLove"
raidText2 = "ratJAM ratJAM The Mischief has arrived! ratJAM ratJAM"

#---------------------------
#   Main methods
#---------------------------
def Init():
    Log("start init")

    EnsureLocalDirectoryExists("Settings")

    Log("finish init")
    return

def Execute(data):

    if not data.IsChatMessage() or not data.IsFromTwitch():
        return

    Clipboard(data)
    ResponseCommands(data)
    ReadResources(data)
    
    return

#---------------------------
#   Helpful methods
#---------------------------

def Tick():
    return
    
def EnsureLocalDirectoryExists(dirName):
    directory = os.path.join(os.path.dirname(__file__), dirName)
    if not os.path.exists(directory):
        os.makedirs(directory)

def SendMessage(message):
    Parent.SendStreamMessage(message)
    return
    
def Log(message):
    Parent.Log("Hooded", str(message))
    return

def isOnCooldown(command, data):
    if(Parent.GetCooldownDuration(ScriptName, command) > 0):
        Log(str(command) + " is still on cooldown (" + str(Parent.GetCooldownDuration(ScriptName, command)) + " seconds)")
        if(MySettings(SettingsFile).NotifyCooldown):
            notifyCooldown(command, data)
        return True
    else:
        return False

def addCooldown(command, data):
    if Parent.HasPermission(data.User,MySettings(SettingsFile).PermissionOverride, MySettings(SettingsFile).Info):
        #Log("Bypass cooldown")
        pass
    elif Parent.HasPermission(data.User,MySettings(SettingsFile).Permission,MySettings(SettingsFile).Info):
        Parent.AddCooldown(ScriptName, command, int(MySettings(SettingsFile).Cooldown * 60))
        #Log("Added cooldown")
        pass
    else:
        pass
    return

def InitRandlineText():
    f = open(resourcesEightBallPath,)
    data = json.load(f)
    eightballMax = 0

    for i in data['Eightball']:
        eightballMax += i['value']

    return eightballMax


#-=-=-=--=-=-=--=-=-=--=-=-=--=-=-=--=-=-=--=-=-=--=-=-=--=-=-=--=-=-=--=-=-=--=-=-=--=-=-=--=-=-=--=-=-=-
#   Custom methods
#-=-=-=--=-=-=--=-=-=--=-=-=--=-=-=--=-=-=--=-=-=--=-=-=--=-=-=--=-=-=--=-=-=--=-=-=--=-=-=--=-=-=--=-=-=-

def Clipboard(data):
    global clipboard
    cb = ""
    
    Parent.BroadcastWsEvent("EVENT_MINE","{'show':false}")
    
    #Copy command, permission and cooldown check
    if data.GetParam(0).lower() == MySettings(SettingsFile).ClipboardCopyCommand \
    and not Parent.IsOnUserCooldown(ScriptName,MySettings(SettingsFile).ClipboardCopyCommand,data.User) \
    and Parent.HasPermission(data.User,MySettings(SettingsFile).ClipboardCopyPermission,MySettings(SettingsFile).ClipboardCopyInfo):
        for x in range (1, data.GetParamCount()):
            cb = cb + data.GetParam(x)
            cb = cb + " "
        clipboard = cb
        SendMessage("Copied!")
        Log("Clipboard: Copied")
        addCooldown(MySettings(SettingsFile).ClipboardCopyCommand, data) 

    #Paste command, permission and cooldown check
    elif data.GetParam(0).lower() == MySettings(SettingsFile).ClipboardPasteCommand \
    and not Parent.IsOnUserCooldown(ScriptName,MySettings(SettingsFile).ClipboardPasteCommand,data.User) \
    and Parent.HasPermission(data.User,MySettings(SettingsFile).ClipboardPastePermission,MySettings(SettingsFile).ClipboardPasteInfo):
        Parent.BroadcastWsEvent("EVENT_MINE","{'show':false}")
        SendMessage(clipboard)
        Log("Clipboard: Pasted")
        addCooldown(MySettings(SettingsFile).ClipboardPasteCommand, data) 
    return

def ReadResources(data):

    if data.GetParam(0).lower() == MySettings(SettingsFile).RandlineTextCommand \
        and not Parent.IsOnUserCooldown(ScriptName,MySettings(SettingsFile).RandlineTextCommand,data.User) \
        and Parent.HasPermission(data.User,MySettings(SettingsFile).RandlineTextPermission,MySettings(SettingsFile).RandlineTextInfo):
            
            Log("8Ball Command: called")
            eightballMax = InitRandlineText();
            SendMessage(str(eightballMax))
            f = open(resourcesEightBallPath,)
            data = json.load(f)

            randomInt = random.randint(0,eightballMax-1)
            SendMessage(str(randomInt))
            for i in data['Eightball']:
                randomInt -= i['value']
                SendMessage(str(randomInt))
                if(randomInt <= 0):
                    SendMessage(i['text'])
                    break

            addCooldown(MySettings(SettingsFile).RandlineTextCommand, data) 

def ResponseCommands(data):

    #Current Spotify Song
    if data.GetParam(0).lower() == MySettings(SettingsFile).CurrentSongCommand \
    and not isOnCooldown(MySettings(SettingsFile).CurrentSongCommand, data):
        
        Log("Current Spotify Song Command: called")
        f = open(songPath, "r")
        SendMessage(f.read())
        addCooldown(MySettings(SettingsFile).CurrentSongCommand, data)   
        
    #Raid command
    if data.GetParam(0).lower() == MySettings(SettingsFile).RaidCommand \
    and not Parent.IsOnUserCooldown(ScriptName,MySettings(SettingsFile).RaidCommand,data.User) \
    and Parent.HasPermission(data.User,MySettings(SettingsFile).RaidPermission,MySettings(SettingsFile).RaidInfo):
        
        Log("Raid Command: called")
        SendMessage(raidText1)
        SendMessage(raidText2)
        addCooldown(MySettings(SettingsFile).RaidCommand, data) 

    #Self made command
    if data.GetParam(0).lower() == "kekw":
        SendMessage("@" + data.UserName + " KEKW")